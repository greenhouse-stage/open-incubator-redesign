<?php

include('functions/cpt.php');
include('functions/tax.php');
include('functions/helpers.php');

add_theme_support( 'post-thumbnails' );

function registerMenus() {

    //registers the main menu for use in the header
    register_nav_menus( array(
        'header_menu'   => 'Header menu',
        'mobile_header_menu' => 'Mobile Header menu',
        'footer_menu'   => 'Footer menu',
    ));
}
add_action('init', 'registerMenus');

//add theme options pages
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'administrator',
		'redirect'		=> false
	));
}

//registers stylesheet
function enqueueStyle() {
	wp_enqueue_style( 'style.css', get_stylesheet_uri());
    wp_enqueue_script('main.js', get_template_directory_uri().'/main.js', array(), "1.0");
}
add_action( 'wp_enqueue_scripts', 'enqueueStyle' );

add_action( 'wp_head', 'get_user_role'); 

function get_user_role() {
 if( is_user_logged_in() ) { // check if there is a logged in user 
	 
	 $user = wp_get_current_user(); // getting & setting the current user 
	 $roles = ( array ) $user->roles; // obtaining the role 
	 
		return $roles[0]; // return the role for the current user 
	 
	 } else {
		 
		return array(); // if there is no logged in user return empty array  
	 
	 }
}
add_filter("walker_nav_menu_start_el", "filter_menu_items",10,4);
function filter_menu_items($output, $item, $depth, $args) {
    if ('header-menu' == $args->theme_location && $depth === 0) {
        if(in_array("menu-item-has-children", $item->classes)) {
            $output = strip_tags($output);
        }
        
    }
    return $output;
}

add_action( 'gform_after_submission', 'createStartup', 10, 2 );
function createStartup($entry, $form) {

    if (get_field('creation_form', 'options') == $entry['form_id']); {
        $inputpos = array_search('transaction_type', array_keys($entry)) + 1;
        $entry_values = array_splice($entry, $inputpos);
    
        $form = GFAPI::get_form( $entry['form_id']);
        $form = $form['fields'];
    
        foreach ($form as $key => $val) {
            $form[$key] = $val->adminLabel;
        }
    
        $output = array();
        foreach ($entry_values as $key => $value) {
            $output[$form[$key]] = $value;
        }
    
        $args = array(
            'post_title' => $output[get_field('post_title_admin_field', 'options')],
            'post_status' => 'draft',
            'post_type' => 'solutions',
            'post_content' => ""
        );
    
        $postid = wp_insert_post($args);

        if($postid != 0){
            try {
                $file_url = parse_url($output[get_field('image_admin_field','options')]);
                $image_id = create_post_attachment($postid, $file_url['path']);
                set_post_thumbnail($postid, $image_id);
                wp_set_post_tags($postid, $output[get_field('categories_admin_field', 'options')]);
                //update_field('solution_tags',$output[get_field('categories_admin_field', 'options')], $postid);
                update_field('short_desc',$output[get_field('description_admin_field','options')], $postid);
                update_field('situation',$output[get_field('situation_admin_field', 'options')], $postid);
                update_field('challenge',$output[get_field('challenge_admin_field', 'options')], $postid);
                update_field('concept',$output[get_field('concept_admin_field', 'options')], $postid);
            } catch(Exeption $e) {
                throw new Exception('Error generating website ' . $e);
            }
            
        } else {
            throw new Exception('Unable to generate post. Please contact your web developer.');
        }
    }

    return $entry;
}