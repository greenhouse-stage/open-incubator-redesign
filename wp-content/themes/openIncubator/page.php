<?php get_header(); ?> 

<div class="content">
	<?php get_template_part('template-parts/blocks'); ?>

	<?php if(get_field('show_connect_button', 'options')) {
		get_template_part('template-parts/connect'); 
	}?>
</div>

<?php get_footer();?>