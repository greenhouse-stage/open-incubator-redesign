<?php 
if(get_field('connect_button_decoration', 'options')){
    $decoration1 = get_field('connect_button_decoration', 'options');
}
?>
<div class="connect">
    <div class="wrap">
        <a href="/connect"><button type="button" class="">Connect ></button></a>
        <div class="connectImg">
            <img src="<?php echo get_field('connect_button_image', 'options') ?>">
        </div>
    </div>
</div>