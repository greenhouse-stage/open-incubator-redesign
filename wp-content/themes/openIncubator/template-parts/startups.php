<?php
$postArgs = array(
    'orderby'        => 'post_date',
    'post_type'      => 'Start-up',
    'posts_per_page' => 3
);

$startups = new WP_Query($postArgs);

if ($startups-> have_posts()) {
    ?> <div class="startupHero">
            <ul> <?php
                $startupNr = 0;
                while ($startups -> have_posts()) {
                    $startups -> the_post();
                    ?> <li class=<?php echo "startup{$startupNr}"?>>
                    <div>
                        <a href= <?php the_permalink() ?>>
                        <?php if(has_post_thumbnail()) {
				            the_post_thumbnail('full');
			            } else {
			                ?> <img class='postImg' src="https://i.stack.imgur.com/y9DpT.jpg" alt="placeholder"> <?php
			            } ?>
                        <img src="<?php echo $url ?>" />
				        <h3><?php the_title() ?></h3>
                        <p class="cta">See more</p>
                        </a>
                    </div>
                    </li>
                    <?php $startupNr = $startupNr + 1;
                } ?>
            </ul>
    </div> <?php
}?>