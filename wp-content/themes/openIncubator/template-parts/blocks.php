<?php

if(have_rows('content_blocks')) {
    while(have_rows('content_blocks') ) {
        the_row();
        if(get_field('show_block_debug', 'options')){
            var_dump(the_row());
        }
        get_template_part('blocks/'. str_replace('_', '-', get_row_layout()));

    } 
}else{
    the_content();
}

?>