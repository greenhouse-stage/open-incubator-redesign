<?php
$postArgs = array(
    'orderby'        => 'post_date',
    'post_type'      => 'offer',
    'posts_per_page' => 3
);

$WhatWeOffer = new WP_Query($postArgs);

if ($WhatWeOffer-> have_posts()) {
    ?> <div class="offersHero">
        <h2>With 15+ years of experience under our belt, we understand what you need as an entrepreneur to succeed. As a platform, we offer:</h2>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud </p>
        <ul> <?php
            while ($WhatWeOffer -> have_posts()) {
                $WhatWeOffer -> the_post();
                ?> <li>
                    <div>
                        <a href= <?php the_permalink() ?>>
                        <?php if(has_post_thumbnail()) {
                            the_post_thumbnail('full');
                        } else {
                            ?> <img class='postImg' src="https://i.stack.imgur.com/y9DpT.jpg" alt="placeholder"> <?php
                        }?>
                        <h3><?php the_title() ?></h3>
                        <p class="cta">See more</p>
                        </a>
                    </div>
                </li>
                <?php 
            } ?>
        </ul>
    </div> <?php
}?>