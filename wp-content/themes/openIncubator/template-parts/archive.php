<?php 

$postArgs = array(
    'post_type'      => $args['post_type'],
);

$query = new WP_Query($postArgs);
if($args['show_categories']) {
	?> <div class="showFilter"><h1 class="showFilter">></h1></div>
	<div class= "postFilter">
		<h3>Categories</h3><?php
		$terms = get_terms( array(
			'taxonomy' => 'industries',
			'hide_empty' => false,
		));
		?><ul><?php
		foreach($terms as $term) {
			if($term->parent != 0){
				?>	<li><a href=<?php echo('/industries/'.$term->slug);?>><button><?php echo("> " . $term->name);?></button></a></li><?php
			} else {
				?>	<li><a href=<?php echo('/industries/'.$term->slug);?>><button><?php echo($term->name);?></button></a></li><?php
			}
				
		}
		?></ul>
	</div> <?php
}
if ($query-> have_posts()) {
	?> 
	<div class='postList'>
	<?php

	while($query -> have_posts()) {
		$query -> the_post();
		?> 
			<div class='post'>
				<a href= <?php the_permalink() ?>>
					<?php 	
					if(has_post_thumbnail()) {
						the_post_thumbnail('full');
					} else {
					?> <img class='postImg' src="https://i.stack.imgur.com/y9DpT.jpg" alt="placeholder"> <?php
					}
					?>
					<div class='description'>
						<h4> <?php the_title() ?> </h4>
						<?php $terms = get_the_terms(get_the_id(), 'industries');?>
						<?php if($terms) {
							?><p class="tags"><?php echo $terms[0]->name ?></p><?php
						} ?>
						<p class="cta">See more</p>
					</div>
				</a>
			</div>
		<?php
	}
	?>
	</div> 
	<?php
}


