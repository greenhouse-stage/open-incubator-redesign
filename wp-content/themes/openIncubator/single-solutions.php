<?php get_header(); ?> 

<div class="content-single">
    <div class="solution-header">
        <a href="/"><button>go back</button></a>
        <?php 
            $industries = get_the_terms(get_the_id(), 'industries');
            if($industries){
                ?> <div class="taxonomies"> <?php
                foreach ($industries as $tag) {
                    ?> <span class="tag"><?php echo $tag->name; ?></span> <?php
                }
                ?> </div> <?php
            } 
        ?> 
        <div class="solution-title"><h1><?php the_title(); ?></h1></div>
        <div class="solution-desc"><p><?php echo get_field('short_desc'); ?></p></div>
    </div>
    <div class="solution-thumbnail"><img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large' ); ?>"></div>
    <div class="solution-situation"><div><h2>Situation</h2><p><?php echo get_field("situation")?></p></div></div>
    <div class="column">
        <div class="solution-challenge"><h2>Challenge</h2><p><?php echo get_field("challenge")?></p></div>
        <div class="solution-concept"><h2>Concept</h2><p><?php echo get_field("concept")?></p></div>
    </div>
</div>
<?php get_template_part('template-parts/connect'); ?>
<?php get_footer();?>