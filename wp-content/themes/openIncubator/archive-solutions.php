<?php 
get_header();
?>
<div class="content-archive">
	<?php 
	get_template_part("/template-parts/archive", null, array(
	'post_type' => 'solutions',
	'show_categories' => true,
	));?>
</div>
<?php



if(get_field('show_connect_button', 'options')) {
	get_template_part('template-parts/connect'); 
}

get_footer();
?>