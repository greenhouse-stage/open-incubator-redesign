document.addEventListener("DOMContentLoaded", () => {
    const showFilter = document.querySelector(".showFilter")
    if(showFilter != null) {
        showFilter.addEventListener("click", function() {
            const filterMenu = document.querySelector('.postFilter');
            //console.log("test");
        
            if (filterMenu.style.display == "block") {
                filterMenu.style.display = "none";
            } else {
                filterMenu.style.display = "block";
            }
        });
    }
    
    document.querySelector(".showMenu").addEventListener("click", function() {
        const filterMenu = document.querySelector('.menu-mobile-header-menu-container');
        //console.log("test");
    
        if (filterMenu.style.display == "block") {
            filterMenu.style.display = "none";
        } else {
            filterMenu.style.display = "block";
        }
    });
})

