<!doctype html>
<header>
	<link
		rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
	/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head() ?>
</header>
<html>
	<body> 

<div class="menuWrap"> 
	<div class ="primaryMenu">
		<div class="logo">
			<img src=<?php the_field('logo_image', 'option');?> alt='open-incubator logo'>
		</div>
		<?php
		wp_nav_menu( $args = array(
			'menu'              => 'Main Menu', //name of the menu set in WP to display
			'theme_location'	=>  'header_menu', //use the menu location defined in functions.php
			'fallback_cb'       => false, // dont fallback on standard wp menu if it fails
			));
		?>
	</div>
	<div class="mobile-menu">
		<div class="showMenu"><h1>></h1></div>
		<?php
			wp_nav_menu( $args = array(
				'menu'              => 'Mobile header Menu', //name of the menu set in WP to display
				'theme_location'	=>  'mobile_header_menu', //use the menu location defined in functions.php
				'fallback_cb'       => false, // dont fallback on standard wp menu if it fails
				));
		?>
		<div class="logo">
			<img src=<?php the_field('logo_image', 'option');?> alt='open-incubator logo'>
		</div>
	</div>
	
</div>

