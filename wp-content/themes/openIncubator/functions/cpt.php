<?php
//function to define the type of custom Case post
function customPostTypes() {

    //array of the texts used in the admin menu
    $labels = array(
        'name'               => _x( 'Solutions', 'post type general name' ),
        'singular_name'      => _x( 'Solution', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'Solution' ),
        'add_new_item'       => __( 'Add New Solution' ),
        'edit_item'          => __( 'Edit Solution' ),
        'new_item'           => __( 'New Solution' ),
        'all_items'          => __( 'All Solutions' ),
        'view_item'          => __( 'View Solutions' ),
        'search_items'       => __( 'Search Solutions' ),
        'not_found'          => __( 'No Solutions found' ),
        'not_found_in_trash' => __( 'No Solutions found in the Trash' ),
        'menu_name'          => 'Solutions'
    );

    //array of post type properties
    $args = array(
        'labels'        => $labels,
        'description'   => 'Solutions shown on the Open-Incubator.',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array('editor', 'title', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'rewrite'       => array('slug' => 'solutions'),
        'has_archive'   => true,
        'taxonomies'    => array( 'industries', 'post_tag' ),

    );
    register_post_type( 'solutions', $args );
}

//adds the custom post function to wordpress on init
add_action( 'init', 'customPostTypes' );

?>