<?php 
function registerTaxonomy() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Industries', 'textdomain'),
        'singular_name'     => _x( 'Industry', 'textdomain'),
        'search_items'      => __( 'Search Industries', 'textdomain'),
        'all_items'         => __( 'All Industries', 'textdomain'),
        'parent_item'       => __( 'Parent Industry', 'textdomain'),
        'parent_item_colon' => __( 'Parent Industry:', 'textdomain'),
        'edit_item'         => __( 'Edit Industry', 'textdomain'),
        'update_item'       => __( 'Update Industry', 'textdomain'),
        'add_new_item'      => __( 'Add New Industry', 'textdomain'),
        'new_item_name'     => __( 'New Industry name', 'textdomain'),
        'menu_name'         => __( 'Industries'),
    );
 
    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'industries' ),
    );
 
    register_taxonomy( 'industries', array( 'industries' ), $args );
}
add_action( 'init', 'registerTaxonomy', 0 );
?>