<?php 
//function to remove empty values from array
function removeEmpty($var) {
    return !($var == "");
}

//Get the child terms of a parent term
function getChildTerms($tax, $parent) {
    $parent_terms = get_terms( $tax, array( 'parent' => 0, 'orderby' => 'slug', 'hide_empty' => false ) );

    foreach ($parent_terms as $pterm) {
        if ($pterm->slug == $parent) {
            $child_terms = get_terms( $tax, array( 'parent' => $pterm->term_id, 'orderby' => 'slug', 'hide_empty' => false ) );
        }
    }
    return $child_terms;
}

//retrieve the slug of the current page (use for dynamic templates)
function getPageName() {
    $urlparts = explode("/", $_SERVER['REQUEST_URI']);
    $pagefolders = array_filter($urlparts, "removeEmpty");
    $pagename = end($pagefolders);

    return $pagename;
}


function create_post_attachment($post_id, $attachment_url){
    // Check the type of file. We'll use this as the 'post_mime_type'.
    $filetype = wp_check_filetype( basename( $attachment_url), null );
    
    
    // Get the path to the upload directory.
    $wp_upload_dir = wp_upload_dir();
    
    // Prepare an array of post data for the attachment.
    $attachment = array(
        'guid'           => $wp_upload_dir['url'] . '/' . basename( $attachment_url ), 
        'post_mime_type' => $filetype['type'],
        'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $attachment_url) ),
        'post_content'   => '',
        'post_status'    => 'inherit'
    );
    
    // Insert the attachment.
    $attach_id = wp_insert_attachment( $attachment, $attachment_url, $post_id );
    
    // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    
    // Generate the metadata for the attachment, and update the database record.
    $attach_data = wp_generate_attachment_metadata( $attach_id, $attachment_url );
    wp_update_attachment_metadata( $attach_id, $attach_data );

    return $attach_id;
}
?>