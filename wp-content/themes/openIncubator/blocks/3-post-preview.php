<?php 
    if(have_rows('3_post_preview_settings', 'options')){
        while(have_rows('3_post_preview_settings', 'options')){
            the_row();

            $decoration1 = get_sub_field('bottom_left_decoration');


        }
    }
?>

<div class="_<?php echo str_replace('_', '-', get_row_layout()) ?>">
    <div class="preview">
        <ul> 
            <?php
            $postArgs = array(
                'orderby'        => get_sub_field('post_order'),
                'order'          => 'ASC',
                'post_type'      => get_sub_field('post_type'),
                'posts_per_page' => 3
            );
            
            $startups = new WP_Query($postArgs);
            
            if ($startups-> have_posts()) {
                while ($startups -> have_posts()) {
                    $startups -> the_post();
                    ?> <li class="hover-border">
                    <div class="listItem">
                        <a href= <?php the_permalink() ?>>
                        <?php if(has_post_thumbnail()) {
                            ?> <div class="imgWrap"> <?php echo the_post_thumbnail('full'); ?> </div> <?php
                        } else {
                            ?> <div class="imgWrap"><img class='postImg' src="https://i.stack.imgur.com/y9DpT.jpg" alt="placeholder"></div>
                        <?php }
                        
                        
                        ?>
                        <h3><?php the_title() ?></h3> 
                        <?php
                        $category = get_the_category();
                        if(!empty($category)) {
                            $category = $category[0];
                            $category = get_object_vars($category);
                            ?>
                            <p class="tag"> - <?php echo $category['name'] ?></p> <?php
                        } ?>
                        <br>
                        <p class="cta">See more</p>
                        </a>
                    </div>
                    </li>
                <?php } ?>
            <?php } 
            wp_reset_postdata();?>
        </ul>
    </div>
    
    <?php if(get_sub_field('image_1') != "") { ?>
        <div class="image-1">
            <img class='postImg' src="<?php echo get_sub_field('image_1'); ?>" alt="image-1">
        </div>
    <?php } ?>

    <div class="text">
        <?php echo get_sub_field('text') ?>
        <?php if(get_sub_field('button_text') != "") { ?>
            <a href="<?php echo get_sub_field('button_link') ?>"><button type="button">> <?php echo get_sub_field('button_text')?></button></a>
        <?php } ?>
    </div>


</div>