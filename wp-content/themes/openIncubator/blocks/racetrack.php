<?php 
    if(have_rows('racetrack_settings', 'options')){
        while(have_rows('racetrack_settings', 'options')){
            the_row();
            $backgroundImage = get_sub_field('background_image');

            $goalImage = get_sub_field('goal_image');
            $focusImage = get_sub_field('focus_image');
            $fitForImage = get_sub_field('fit_for_image');
            $resultImage = get_sub_field('result_image');
            $focusOnImage = get_sub_field('focus_on_image');
            $kpiImage = get_sub_field('kpi_image');
            $timingImage = get_sub_field('timing_image');
            $resourcesImage = get_sub_field('resources_image');
        }
    }
?>

<div class="<?php echo str_replace('_', '-', get_row_layout()) ?>">
<div class="previous">
            <a href=<?php echo($url .'?phase=' .$previousPhase); ?> alt="previous"><h2><</h2></a>
        </div>
    <div class="racetrack-graph">
        <div class="background-image">
            <img src="<?php echo $backgroundImage ?>" alt="background">
        </div>

        <div class="phase-1">
            <div class="phase-heading">
                <h2><?php echo get_sub_field("title_1"); ?></h2>
            </div>
            <div class="phase-summary">
                <p>#1</p>
                <p><?php echo get_sub_field("summary_1"); ?></p>
            </div>
        </div>
        <div class="phase-2">
            <div class="phase-heading">
                <h2><?php echo get_sub_field("title_2"); ?></h2>
            </div>
            <div class="phase-summary">
                <p>#2</p>
                <p><?php echo get_sub_field("summary_2"); ?></p>
            </div>
        </div>
        <div class="phase-3">
            <div class="phase-heading">
                <h2><?php echo get_sub_field("title_3"); ?></h2>
            </div>
            <div class="phase-summary">
                <p>#3</p>
                <p><?php echo get_sub_field("summary_3"); ?></p>
            </div>
        </div>
        <div class="phase-4">
            <div class="phase-heading">
                <h2><?php echo get_sub_field("title_4"); ?></h2>
            </div>
            <div class="phase-summary">
                <p>#4</p>
                <p><?php echo get_sub_field("summary_4"); ?></p>
            </div>
        </div>
    </div>
<?php
    $phase = null;
if(isset($_GET['phase'])){
    $phase = $_GET['phase'];
} else {
	$phase = 1;
}

$url = strtok($_SERVER['REQUEST_URI'], '?');

$nextPhase = $phase + 1;
$previousPhase = $phase - 1;

if($phase > 3) {
 $nextPhase = 4;
}elseif($phase < 2) {
    $previousPhase = 1;
}

?>
    <div class="racetrack-table">
        <table class="table">
            <tr>
                <th style="text-align: left;"><h2>#<?php echo $phase ?> <?php echo get_sub_field('title_' .$phase); ?></h2> </th>
            </tr>
            <tr>
                <td><h3>Our goal</h3><p><?php echo get_sub_field('goal_' .$phase); ?></p></td>
            </tr>
            <tr>
                <td><h3>Our focus</h3><p><?php echo get_sub_field('focus_' .$phase); ?></p></td>
            </tr>
            <tr>
                <td><h3>Fit for</h3><p><?php echo get_sub_field('fit_for_' .$phase); ?></p></td>
            </tr>
            <tr>
                <td><h3>Results</h3><p><?php echo get_sub_field('end_result_' .$phase); ?></p></td>
            </tr>
            <tr>
                <td><h3>Focused on</h3><p><?php echo get_sub_field('focus_on_' .$phase); ?></p></td>
            </tr>
            <tr>
                <td><h3>KPI</h3><p><?php echo get_sub_field('kpi_' .$phase); ?></p></td>
            </tr>
            <tr>
                <td><h3>Timing</h3><p><?php echo get_sub_field('timing_' .$phase); ?></p></td>
            </tr>
            <tr>
                <td><h3>Resources provided</h3><p><?php echo get_sub_field('resources_' .$phase); ?></p></td>
            </tr>
        </table>
    </div>
    <div class="next">
        <a href=<?php echo($url .'?phase=' .$nextPhase); ?> alt="next"><h2>></h2></a>
    </div>
</div>
