<?php 
    if(have_rows('image_hero_2_images_settings', 'options')){
        while(have_rows('image_hero_2_images_settings', 'options')){
            the_row();

            $decoration1 = get_sub_field('bottom_right_decoration');
            $racetrack = get_sub_field('racetrack_decoration');


        }
    }
?>

<div class="<?php echo str_replace('_', '-', get_row_layout()) ?>">
    <?php if(get_sub_field('image_1') != "") { ?>
        <div class="image-1">
            <img class='postImg' src="<?php echo get_sub_field('image_1'); ?>" alt="image-1">
        </div>
    <?php } ?>
    <div class="text">
        <?php echo get_sub_field('text') ?>
        <?php if(get_sub_field('button_text') != "") { ?>
            <a href="<?php echo get_sub_field('button_link') ?>"><button type="button">> <?php echo get_sub_field('button_text')?></button></a>
        <?php } ?>
    </div>

    <?php if(get_sub_field('image_2') != "") { ?>
        <div class="image-2">
            <img class='postImg' src="<?php echo get_sub_field('image_2'); ?>" alt="image-2">
        </div>
    <?php } ?>
    <?php if(get_sub_field('image_3') != "") { ?>
        <div class="image-3">
            <img class='postImg' src="<?php echo get_sub_field('image_3'); ?>" alt="image-3">
        </div>
    <?php } ?>


    <div class="racetrack">
            <img src="<?php echo $racetrack ?>" alt="racetrack">
    </div>
            
</div>
