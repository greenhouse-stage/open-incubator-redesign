<div class="_<?php echo str_replace('_', '-', get_row_layout()) ?>">
    <div class="column">
        <?php if(get_sub_field('image_1') != ""){ ?>
            <div class="image"><img src="<?php echo get_sub_field("image_1") ?>" alt="image-1"></div>
        <?php } ?>
        <?php if(get_sub_field('text_1') != ""){ ?>
            <div class="text"><?php echo get_sub_field("text_1") ?></div>
        <?php } ?>
    </div>
    <div class="column">
        <?php if(get_sub_field('image_2') != ""){ ?>
            <div class="image"><img src="<?php echo get_sub_field("image_2") ?>" alt="image-2"></div>
        <?php } ?>
        <?php if(get_sub_field('text_2') != ""){ ?>
            <div class="text"><?php echo get_sub_field("text_2") ?></div>
        <?php } ?>
    </div>
    <div class="column">
        <?php if(get_sub_field('image_3') != ""){ ?>
            <div class="image"><img src="<?php echo get_sub_field("image_3") ?>" alt="image-3"></div>
        <?php } ?>
        <?php if(get_sub_field('text_3') != ""){ ?>
            <div class="text"><?php echo get_sub_field("text_3") ?></div>
        <?php } ?>
    </div>
</div>