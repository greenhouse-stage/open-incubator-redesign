<div class="<?php echo str_replace('_', '-', get_row_layout()) ?>">
<div> <?php the_content(); ?></div>
    <?php if(have_rows('members')){?>
        <ul>
        <?php while(have_rows('members')){ 
            the_row();?>
            <li>
                <div class="image"><img src="<?php echo get_sub_field('image'); ?>" alt="image"></div>
                <div class="text"><strong><?php echo get_sub_field('name');?></strong><h3><?php echo get_sub_field('title');?></h3><?php echo get_sub_field('description');?>
            </li>
        <?php } ?>
        </ul>
   <?php } ?>
</div>