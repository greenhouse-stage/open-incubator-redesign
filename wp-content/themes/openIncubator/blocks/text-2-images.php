

<div class="<?php echo str_replace('_', '-', get_row_layout()) ?>">
<div class="text">
        <?php echo get_sub_field('text') ?>
        <?php if(get_sub_field('button_text') != "") { ?>
            <a href="<?php echo get_sub_field('button_link') ?>"><button type="button">> <?php echo get_sub_field('button_text')?></button></a>
        <?php } ?>
    </div>

    <?php if(get_sub_field('image_1') != "") { ?>
        <div class="image-1">
            <img class='postImg' src="<?php echo get_sub_field('image_1'); ?>" alt="image-1">
        </div>
    <?php } ?>

    <?php if(get_sub_field('image_2') != "") { ?>
        <div class="<?php if(get_sub_field('align_right')){ echo "image-2-right";}else{echo "image-2";}?>">
            <img class='postImg' src="<?php echo get_sub_field('image_2'); ?>" alt="image-2">
        </div>
    <?php } ?>

     
</div>
