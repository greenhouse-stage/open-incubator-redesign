<div class="_<?php echo str_replace('_', '-', get_row_layout()) ?>">
    <div class="text">
        <?php echo get_sub_field('text') ?>
        <?php if(get_sub_field('button_text') != "") { ?>
            <a href="<?php echo get_sub_field('button_link') ?>"><button type="button">> <?php echo get_sub_field('button_text')?></button></a>
        <?php } ?>
    </div>
    <ul>
        <li>
            <?php if(get_sub_field('image_1') != "") { ?>
                <div class="image"><img src="<?php echo get_sub_field('image_1'); ?>" alt="offer 1"></div>
            <?php } ?>
            <?php if(get_sub_field('title_1') != "") { ?>
                <p><?php echo get_sub_field('title_1'); ?></p>
            <?php } ?>
        </li>
        <li>
            <?php if(get_sub_field('image_2') != "") { ?>
                <div class="image"><img src="<?php echo get_sub_field('image_2'); ?>" alt="offer 2"></div>
            <?php } ?>
            <?php if(get_sub_field('title_2') != "") { ?>
                <p><?php echo get_sub_field('title_2'); ?></p>
            <?php } ?>
        </li>
        <li>
            <?php if(get_sub_field('image_3') != "") { ?>
                <div class="image"><img src="<?php echo get_sub_field('image_3'); ?>" alt="offer 3"></div>
            <?php } ?>
            <?php if(get_sub_field('title_3') != "") { ?>
                <p><?php echo get_sub_field('title_3'); ?></p>
            <?php } ?>
        </li>
        <li>
            <?php if(get_sub_field('image_4') != "") { ?>
                <div class="image"><img src="<?php echo get_sub_field('image_4'); ?>" alt="offer 4"></div>
            <?php } ?>
            <?php if(get_sub_field('title_4') != "") { ?>
                <p><?php echo get_sub_field('title_4'); ?></p>
            <?php } ?>
        </li>
    </ul>
</div>