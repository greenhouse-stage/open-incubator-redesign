<?php 
    if(have_rows('hero_3_images_settings', 'options')){
        while(have_rows('hero_3_images_settings', 'options')){
            the_row();

            $decoration1 = get_sub_field('bottom_left_decoration');
            $decoration2 = get_sub_field('bottom_center_decoration');
            $signedAmount = get_sub_field('signed_up_amount');
            $racetrack = get_sub_field('racetrack_decoration');

        }
    }
?>


<div class="<?php echo str_replace('_', '-', get_row_layout()) ?>">
    <div class="text">
        <?php echo get_sub_field('text') ?>
        <?php if(get_sub_field('button_text') != "") { ?>
            <a href="<?php echo get_sub_field('button_link') ?>"><button type="button">> <?php echo get_sub_field('button_text')?></button></a>
        <?php } ?>
    </div>

    <div class="image-1">
        <img src="<?php echo get_sub_field('image_1'); ?>" alt="image-1">
    </div>

    <?php if(get_sub_field('image_2') != "") { ?>
        <div class="image-2">
            <img class='postImg' src="<?php echo get_sub_field('image_2'); ?>" alt="image-2">
        </div>
    <?php } ?>

    <?php if(get_sub_field('image_3') != "") { ?>
        <div class="image-3">
            <img class='postImg animate__animated animate_fadeInLeft animate__delay-2s' src="<?php echo get_sub_field('image_3'); ?>" alt="image-3">
        </div>
    <?php } ?>
    
    <div class="racetrack">
            <img src="<?php echo $racetrack ?>" alt="racetrack">
    </div>

</div>
