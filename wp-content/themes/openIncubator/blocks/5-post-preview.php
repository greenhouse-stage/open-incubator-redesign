<div class="_<?php echo str_replace('_', '-', get_row_layout()) ?>">
    <div class="text">
        <?php echo get_sub_field('text') ?>
        <?php if(get_sub_field('button_text') != "") { ?>
            <a href="<?php echo get_sub_field('button_link') ?>"><button type="button">> <?php echo get_sub_field('button_text')?></button></a>
        <?php } ?>
    </div>

    <div class="preview">
        <ul> 
            <?php       

            for ($i=1; $i <= 5; $i++) { ?>
                <li class="hover-border">
                    <div class="listItem">
                        <a href= <?php echo get_sub_field('post_'.$i.'_link') ?>>
                            <div class="imgWrap" ><img src=<?php echo get_sub_field('post_'.$i.'_image'); ?>> </div>
                            <h3><?php echo get_sub_field('post_'.$i.'_title') ?></h3>
                        </a>
                    </div>
                </li> <?php
            } ?>
       </ul>
    </div>
</div>