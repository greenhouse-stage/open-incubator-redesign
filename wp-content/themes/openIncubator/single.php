<?php
get_header(); 
if(get_post_type() == 'startup') {
 ?> <div class="acf-fields">
        <div class="startup-title"> <?php echo get_field('startup-name') ?> </div>
        <div class="startup-image"> <?php echo wp_get_attachment_image(get_field('startup-image'),'full');  ?> </div>
        <div class="startup-info"> 
            <ul>
                <li><p><strong>Achievements</strong></p><h2><?php echo get_field('startup-achievements') ?></h2> </li>
                <li><p><strong>Start Date</strong></p><h2><?php echo get_field('startup-date') ?></h2> </li>
                <li><p><strong>Location</strong></p><h2><?php echo get_field('startup-location') ?></h2> </li>
                <li><p><strong>Phase</strong></p><h2><?php echo get_field('startup-phase') ?></h2> </li>
                <li><p><strong>Contact</strong></p><h3><?php echo get_field('startup-contact') ?></h3> </li>
                <li><p><strong>Website</strong></p><h3><?php echo get_field('startup-website') ?></h3> </li>
                
        </div>
    </div> 
    <div class="startup-content"> <?php the_content(); ?> </div> <?php
} else {
   ?><div class="content"> <?php the_content(); ?> </div> <?php 
}

get_template_part('template-parts/connect');
get_footer(); ?>