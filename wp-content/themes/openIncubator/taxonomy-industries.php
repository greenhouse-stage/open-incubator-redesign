<?php get_header();

$pagename = getPageName();

$args = array(
    'post_type' => 'solutions',
    'tax_query' => array(
        array(
            'taxonomy' => 'industries',
            'field'    => 'slug',
            'terms'    => $pagename,
        ),
    ),
);

$query = new WP_Query( $args );
?>
<div class="content industries"> 
    <div class="filters">
        <h3>Filter</h3>
        <ul class="categories">
        <?php
        $childTerms = getChildTerms('industries', $pagename);
        foreach ($childTerms as $term) {
            echo "<button>" . $term->name . "</button>";
        }
        ?>
        </ul>
    </div>
    <div class="items">
        <div class="title">
            <h1><?php echo "all " . $pagename . " start-ups"?></h1>
        </div>
        <?php
        if (have_posts($query)) {
            while (have_posts($query)) {
                the_post();
                ?>
                <h3><?php the_title(); ?></h3>
                <?php
            }
        }?>
    </div>
</div>

<?php get_footer();?>